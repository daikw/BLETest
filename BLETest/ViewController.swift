//
//  ViewController.swift
//  BLETest
//
//  Created by Daiki Watanabe on 2017/11/04.
//  Copyright © 2017年 Daiki Watanabe. All rights reserved.
//

import UIKit
import CoreBluetooth

class ViewController: UIViewController, UITextFieldDelegate, CBCentralManagerDelegate, CBPeripheralDelegate {

    // MARK: - Properties
    var centralManager: CBCentralManager!
    var peripheral: CBPeripheral!
    var targetCharacteristic: CBCharacteristic!
    
    @IBOutlet weak var characteristicTextField: UITextField!
    
    
    // MARK: - Default methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        centralManager = CBCentralManager(delegate: self, queue: nil)
        characteristicTextField.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //  required: Called whenever the connection state changes.
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        print ("state: \(central.state)")
    }

    
    // MARK: - Actions
    
    // Start Scan
    @IBAction func startScan(_ sender: UIButton) {
        
        centralManager.scanForPeripherals(withServices: nil, options: nil)
    }
    
    // Stop Scan
    @IBAction func stopScan(_ sender: UIButton) {
        centralManager.stopScan()
    }

    // Discover services in connected peripheral
    @IBAction func fetchServiceList(_ sender: UIButton) {
        // Set delegate to get the list of services.
        peripheral.delegate = self
        
        // Start to discover services.
        peripheral.discoverServices(nil)
    }
    
    // Send and write the text
    @IBAction func sendText(_ sender: UIButton) {
        guard let text = characteristicTextField.text,
              let data = text.data(using: .utf8) else {
                
            print("Invalid text as characteristic")
            return
        }
        
        if let target = targetCharacteristic {
            peripheral.writeValue(data, for: target, type: CBCharacteristicWriteType.withResponse)
        }
    }

    
    // MARK: - UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // Hide the keyboard.
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
    }
    
    
    // MARK: - Event handlers of the central manager
    
    // Called when any peripheral is discovered.
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        print("Discovered BLE device: \(peripheral)")
        print("RSSI: \(RSSI)")
        
        // Start connection
        self.peripheral = peripheral
        self.centralManager.connect(self.peripheral, options: nil)
    }
    
    // Called when connection succeeded.
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        print("Connect success!")
        
    }
    
    // Called when connection failed.
    func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) {
        print("Connect failed...")
    }
    
    
    // MARK: - Event handlers of connected peripheral
    
    // Called when services were discovered.
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        guard let services = peripheral.services else{
            print("Services were not discovered.")
            return
        }
        // print("\(services.count) services were discovered in the peripheral: \(peripheral)")
        
        // Discover characteristics from the discovered services.
        for service in services {
            if service.uuid.isEqual(CBUUID(string: "EC00")) {
                peripheral.discoverCharacteristics(nil, for: service)
            }
        }
    }
    
    // Called when characteristics were discovered.
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        guard let characteristics = service.characteristics else {
            fatalError()
        }
        // print("\(characteristics.count) characteristics were discovered in the service: \(service)")

        for characteristic in characteristics {
            if characteristic.uuid.isEqual(CBUUID(string: "EC0E")) {
                targetCharacteristic = characteristic
            }
        }
    }
    
    // Called when reading the characteristic were finished.
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        print("Reading characteristic was successfull!)")
        print("Service UUID: \(characteristic.service.uuid)")
        print("Characteristic UUID: \(characteristic.uuid)")
        print("Characteristic UUID: \(characteristic.uuid)")
        print("value: \(String(describing: characteristic.value))")
    }
    
    // Called when writing the characteristic were finished.
    func peripheral(_ peripheral: CBPeripheral, didWriteValueFor characteristic: CBCharacteristic, error: Error?) {
        print("Writing characteristic was successfull!")
    }
}

